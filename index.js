const morgan = require("morgan");
const bodyParser = require("body-parser");
const express = require("express");
const app = express();
const { Sequelize } = require("sequelize");
const dbConfig = require("./config/appConfig");

app.use(express.static("public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set("view engine", "ejs");
if (app.get("env") === "development") {
    app.use(morgan("tiny"));
}

const sequelize = new Sequelize(
    dbConfig.database.dbname,
    dbConfig.database.username,
    dbConfig.database.password,
    {
        host: dbConfig.database.host,
        dialect: dbConfig.database.dialect,
    }
);

sequelize.sync().then(() => {
    console.log(`Connected to the database.`);
});

// Route
app.use("/api/v1", require("./router/index"));

app.get("/", (req, res) => {
    const data = {
        title: "Login",
        status: null,
        message: null,
    };
    res.render("auth/login", data);
});

const PORT = process.env.NODE_ENV || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
