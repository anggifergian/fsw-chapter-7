const db = require("../models");
const bcrypt = require("bcrypt");

module.exports.auth = async (req, res) => {
    const data = {
        title: "Login",
        status: 400,
    };

    if (!req.body.email || !req.body.password) {
        const message = { ...data, message: `Email or password required` };
        return res.render("auth/login", message);
    }

    try {
        const user = await db.User.findOne({
            where: { email: req.body.email },
        });
        if (!user) {
            const message = { ...data, message: `Invalid email or password` };
            return res.render("auth/login", message);
        }

        const validPassword = await bcrypt.compare(
            req.body.password,
            user.password
        );
        if (!validPassword) {
            const message = { ...data, message: "Invalid email or password" };
            return res.render("auth/login", message);
        }

        const token = user.generateAuthToken();
        res.header("x-auth-token", token).send(
            "Success login. Please look at response headers to check the auth token."
        );
    } catch (err) {
        console.log(err);
    }
};

module.exports.whoami = async (req, res) => {
    console.log(req.user);
};
