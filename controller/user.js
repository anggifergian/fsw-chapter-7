const { User } = require("./../models");
const bcrypt = require("bcrypt");

module.exports.register = async (req, res) => {
    const data = {
        title: "Login",
        status: 400,
        message: `Please fill the name, email, and password`,
    };

    if (!req.body.name || !req.body.email || !req.body.password) {
        const message = {
            ...data,
            message: `Please fill the name, email, and password`,
        };
        return res.render("auth/register", message);
    }

    try {
        let user = await User.findOne({ where: { email: req.body.email } });
        if (user) {
            const message = { ...data, message: "User already registered" };
            return res.render("auth/register", message);
        }

        user = await User.create({
            name: req.body.name,
            email: req.body.email,
            role: req.body.role,
            password: await bcrypt.hash(req.body.password, 12),
            createdAt: new Date(),
            updatedAt: new Date(),
        });

        const token = user.generateAuthToken();
        res.header("x-auth-token", token).send(
            "Successfully register new account. Please look at response headers to check the auth token."
        );
    } catch (err) {
        console.log(err);
    }
};
