"use strict";
const { Model } = require("sequelize");
const jwt = require("jsonwebtoken");
const appConfig = require("./../config/appConfig");

module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        static associate(models) {
            // define association here
        }

        generateAuthToken = () => {
            const payload = {
                id: this.id,
                name: this.name,
                email: this.name,
                role: this.role,
            };
            return jwt.sign(payload, appConfig.jwtSecret);
        };
    }
    User.init(
        {
            name: DataTypes.STRING,
            email: DataTypes.STRING,
            password: DataTypes.STRING,
            role: {
                type: DataTypes.STRING,
                defaultValue: "player",
            },
        },
        {
            sequelize,
            modelName: "User",
        }
    );
    return User;
};
