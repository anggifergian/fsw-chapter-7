const express = require("express");
const router = express.Router();
const auth = require("./../middleware/auth");

// GET - LOGIN
router.get("/login", (req, res) => {
    const data = {
        title: "Login",
        status: null,
        message: null,
    };
    res.render("auth/login", data);
});

// GET - REGISTER
router.get("/register", (req, res) => {
    const data = {
        title: "Register",
        status: null,
        message: null,
    };
    res.render("auth/register", data);
});

// GET - WHOAMI
router.get("/whoami", auth, require("./../controller/auth").whoami);

// POST - LOGIN
router.post("/login", require("./../controller/auth").auth);

// POST - REGISTER
router.post("/register", require("./../controller/user").register);

module.exports = router;
